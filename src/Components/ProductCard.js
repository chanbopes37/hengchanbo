import React from 'react'
import "../styles/ProductCard.css"
import { useNavigate } from 'react-router-dom'
const ProductCard = ({productData}) => {
  
  let navigate = useNavigate();
  const handleLongText =(text ,number)=>{
    if(text.length>number){
      return text.substring(0,number)+'...';
    }
    return text;
  }
  return (
    <div className='pt-4'>
      <div class="product-card ">
        <img className='img-fluid product-image' src={productData.images[0]}
        onError ={({currentTarget})=>{
          currentTarget.onerror= null;
          currentTarget.src = "https://i0.wp.com/thinkfirstcommunication.com/wp-content/uploads/2022/05/placeholder-1-1.png?fit=1200%2C800&ssl=1"
        }}
        
        alt="Product Image" />
        <div class="product-info">
            <h3 class="product-title">{handleLongText(productData.title,15)}</h3>
            <p class="product-description">{handleLongText(productData.description,30)}</p>
            <span class="product-price">${productData.price}</span>
            <button class="product-button"
               onClick={()=>{
                navigate (`/menu/${productData.id}`);
               }}
            >Order</button>
        </div>
        </div>
    </div>
  )
}

export default ProductCard
